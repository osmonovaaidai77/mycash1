from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Contact(models.Model):
    name = models.CharField(max_length=50)
    number = models.CharField (max_length=15)
    user = models.ForeignKey (User, on_delete=models.CASCADE)
    debit = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    credit = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.name} ({self.number})' 

TRANSACTION_CHOICE = (
    ('borrow', 'Взять в долг'),
    ('repay', 'Погасить займ'),
    ('lend', 'Дать в долг'),
    ('receive', 'Принять погашение'),
)

class Transaction(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    transaction_type = models.CharField(
        max_length=20, choices=TRANSACTION_CHOICE, default='borrow',)

TELEGRAM_ACTION_CHOICE = (
    ('add_contact', 'Добавить контакт'),
    ('borrow', 'Взять в долг'),
    ('repay', 'Погасить займ'),
    ('lend', 'Дать в долг'),
    ('receive', 'Принять погашение'),
    ('register', 'Регистрация')
)

class TelegramMessageId(models.Model):
    chat_id = models.CharField(max_length=20)
    message_id = models.CharField(max_length=20)
    action = models.CharField (
        max_length=20, choices=TELEGRAM_ACTION_CHOICE, default='borrow')
    contact_id = models.IntegerField(blank=True, null=True)