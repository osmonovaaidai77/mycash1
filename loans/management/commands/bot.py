from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from loans.models import TelegramMessageId, Contact, Transaction
from loans.bot.math import total_sum

from telebot import TeleBot

from telebot import types
from loans.bot.buttons import main_keyboard, detail_keyboard


User = get_user_model()

bot = TeleBot(settings.TELEGRAM_BOT_TOKEN, threaded=False)


def t_m_creator(chat_id, message_id, action, contact_id):
    TelegramMessageId.objects.create(
        chat_id=chat_id,
        message_id=message_id,
        action=action,
        contact_id=contact_id,)
    
def process_search_contact(user_id, message_text):
        search_query = message_text.strip()
        contacts = Contact.objects.filter(user_id=user_id).filter(
            Q(name__istartswith=search_query) | Q(number__istartswith=search_query))
        return contacts


class Command(BaseCommand):
    help = 'MyCash'

    def handle(self, *args, **options):
        bot.enable_save_next_step_handlers(delay=2)
        bot.load_next_step_handlers()
        bot.infinity_polling()


    @bot.message_handler(commands=['start'])
    def start(message):
        user = User.objects.filter(chat_id=message.chat.id)
        if not user:
            User.objects.create(username=f'{message.chat.username}:{message.chat.id}', chat_id=message.chat.id)
        keyboard = main_keyboard()
        bot.send_message(message.chat.id, f'Вы зарегистрированы как {message.chat.username}', reply_markup=keyboard)

    # @bot.message_handler(commands=['repay'])
    # def repay(message):
    #     bot.reply_to(
    #         message,
    #         """You want to repay"""
    #     )
    # @bot.message_handler(commands=['lend'])
    # def lend(message):
    #     bot.reply_to(
    #         message,
    #         """You want to lend"""
    #     )
    # @bot.message_handler(commands=['receive']
    # def receive(message):
    #     bot.reply_to(
    #         message,
    #         """You want to receive"""
    #     )


    @bot.message_handler(commands=['add_contact'])
    def add_contact(message):
        sent_message = bot.send_message(
            message.chat.id,
            """To add new contact replay to this message: name:number""")
        TelegramMessageId.objects.create(
            chat_id=message.chat.id,
            message_id=sent_message.message_id,
            action='add_contact',)
        print (sent_message.message_id)


    @bot.message_handler()
    def handle_message (message):
        user = User.objects.get(chat_id=message.chat.id)

        if message.text == 'Credit':
           contacts = Contact.objects.filter(user=user).exclude(credit=0).order_by('credit')
           keyboard = types.InlineKeyboardMarkup(row_width=2)
           total = total_sum(user.id, type='credit')
           for contact in contacts:
               contact_button = f'{contact.name}-{contact.credit}'
               button = types.InlineKeyboardButton(text=contact_button, callback_data=f"{contact.id}:detail")
               keyboard.add(button)
           bot.send_message(message.chat.id, f"""Choose the contact. \nTotal:{total}""",  reply_markup=keyboard)
        
        if message.text == 'Debit':
            contacts = Contact.objects.filter(user=user).exclude(debit=0).order_by('debit')
            keyboard = types.InlineKeyboardMarkup(row_width=2)
            total = total_sum(user.id, type='debit')
            for contact in contacts:
               contact_button = f'{contact.name}-{contact.debit}'
               button = types.InlineKeyboardButton(text=contact_button, callback_data=f"{contact.id}:detail")
               keyboard.add(button)
            bot.send_message(message.chat.id, f"""Choose the contact. \nTotal:{total}""",  reply_markup=keyboard)

        if message.text == 'Contacts':
            contacts = Contact.objects.filter(user=user)
            keyboard = types.InlineKeyboardMarkup(row_width=2)
            for contact in contacts:
               contact_button = f'{contact.name}-{contact.number}'
               button = types.InlineKeyboardButton(text=contact_button, callback_data=f"{contact.id}:detail")
               keyboard.add(button)
            bot.send_message(message.chat.id, 'Choose the contact',  reply_markup=keyboard)

        if message.text == '+ Contact':
            sent_message = bot.send_message(
                message.chat.id,
                """To add new contact replay to this message: name:number""")
            TelegramMessageId.objects.create(
                chat_id=message.chat.id,
                message_id=sent_message.message_id,
                action='add_contact')
        
        if message.text == 'Search':
            user = User.objects.get(chat_id=message.chat.id)
            sent_message = bot.send_message(
                message.chat.id,
                'Введите имя или номер контакта для поиска:')
            TelegramMessageId.objects.create(
                chat_id=message.chat.id,
                message_id=sent_message.message_id,
                action='search_contact',)

        if message.reply_to_message:
            replied_message_id = message.reply_to_message.message_id
            saved_message = TelegramMessageId.objects.get (
                message_id=replied_message_id
            )
            if saved_message:
                user = User.objects.get(chat_id=message.chat.id)
                if saved_message.action == 'add_contact':
                    name, number = message.text.split(':')
                    Contact.objects.create(
                        name=name, number=number, user=user
                    )
                    bot.send_message(message.chat.id, 'Contact is created')

                elif saved_message.action == 'borrow':
                    contact = Contact.objects.get(id=saved_message.contact_id)
                    Transaction.objects.create(
                      transaction_type='borrow',
                      contact_id = saved_message.contact_id,
                      amount = int(message.text))
                    contact.credit = contact.credit+int(message.text)
                    contact.save()
                    bot.send_message(message.chat.id, 'Credit is changed')
                elif saved_message.action == 'repay':
                    contact = Contact.objects.get(id=saved_message.contact_id)
                    Transaction.objects.create(
                      transaction_type='repay',
                      contact_id = saved_message.contact_id,
                      amount = int(message.text))
                    contact.credit = contact.credit-int(message.text)
                    contact.save()
                    bot.send_message(message.chat.id, 'Credit is changed')
                elif saved_message.action == 'lend':
                    contact = Contact.objects.get(id=saved_message.contact_id)
                    Transaction.objects.create(
                      transaction_type='lend',
                      contact_id = saved_message.contact_id,
                      amount = int(message.text))
                    contact.debit = contact.debit+int(message.text)
                    contact.save()
                    bot.send_message(message.chat.id, 'Debit is changed')
                elif saved_message.action == 'receive':
                    contact = Contact.objects.get(id=saved_message.contact_id)
                    Transaction.objects.create(
                      transaction_type='receive',
                      contact_id = saved_message.contact_id,
                      amount = int(message.text))
                    contact.debit = contact.debit-int(message.text)
                    contact.save()
                    bot.send_message(message.chat.id, 'Debit is changed')
                elif saved_message.action == 'search_contact':
                    contacts = process_search_contact(user.id, message.text)
                    if contacts.exists():
                        keyboard = types.InlineKeyboardMarkup(row_width=2)
                        for contact in contacts:
                           contact_button = f'{contact.name}-{contact.number}'
                           button = types.InlineKeyboardButton(
                               text=contact_button, callback_data=f'{contact.id}:detail')
                           keyboard.add(button)
                        bot.send_message(message.chat.id, 'Results', reply_markup=keyboard)
                    else:
                        bot.send_message(message.chat.id, 'Contact is not found')

        bot.reply_to(
            message,
            f'Консультант: {message.text}'
        )

    @bot.callback_query_handler(func=lambda call: True)
    def handle_callback(call):
            contact_id, action = call.data.split(':')
            contact = Contact.objects.get(id=int(contact_id))
            action_message = "Replay to this message and write the amount"
            if action == 'detail':
                keyboard = detail_keyboard(contact)
                bot.send_message(call.message.chat.id, f'{contact.name}-{contact.number}',  reply_markup=keyboard)

            elif action == 'borrow':
                sent_message = bot.send_message(
                    call.message.chat.id, action_message)
                t_m_creator(call.message.chat.id, sent_message.message_id, 'borrow', contact.id)
            elif action == 'repay':
                sent_message = bot.send_message(
                    call.message.chat.id, action_message)
                t_m_creator(call.message.chat.id, sent_message.message_id, 'repay', contact.id)
            elif action == 'lend':
                sent_message = bot.send_message(
                    call.message.chat.id, action_message)
                t_m_creator(call.message.chat.id, sent_message.message_id, 'lend', contact.id)
            elif action == 'receive':
                sent_message = bot.send_message(
                    call.message.chat.id, action_message)
                t_m_creator(call.message.chat.id, sent_message.message_id, 'receive', contact.id)