from django.urls import path
from .views import create_contact, contact_list, contact_detail, create_transaction

app_name='loans'

urlpatterns = [
    path ('create_contact', create_contact, name='create_contact'),
    path ('contacts_list', contact_list, name='contacts_list'),
    path ('contact/<int:id>', contact_detail, name='contact_detail'),
    path ('transaction/<str:type>/', create_transaction, name='create_transaction'),
]