from loans.models import Contact
from django.contrib.auth import get_user_model
from django.db.models import Sum

User = get_user_model()

def total_sum(user_id, type):
    filtered_contacts = Contact.objects.filter(user_id=user_id)
    total_sum = filtered_contacts.aggregate(total=Sum(type))['total']
    return total_sum