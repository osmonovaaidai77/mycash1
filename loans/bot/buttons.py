from telebot import types


def main_keyboard():
    keyboard = types.ReplyKeyboardMarkup(row_width=2)
    button1 = types.KeyboardButton('Debit')
    button2 = types.KeyboardButton('Credit')
    button3 = types.KeyboardButton('Contacts')
    button4 = types.KeyboardButton('+ Contact')
    button5 = types.KeyboardButton('Search')
    keyboard.add(button1, button2, button3, button4, button5)
    return keyboard


def detail_keyboard(contact):
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    borrow = types.InlineKeyboardButton(text='+', callback_data=f'{contact.id}:borrow')
    repay = types.InlineKeyboardButton(text='-', callback_data=f'{contact.id}:repay')
    lend = types.InlineKeyboardButton(text='+', callback_data=f'{contact.id}:lend')
    receive = types.InlineKeyboardButton(text='-', callback_data=f'{contact.id}:receive')
    debit = types.InlineKeyboardButton(text=f'Debit:{contact.debit}', callback_data=f'invalid')
    credit = types.InlineKeyboardButton(text=f'Credit:{contact.credit}', callback_data=f'invalid')
    keyboard.add(debit)
    keyboard.add(receive, lend)
    keyboard.add(credit)
    keyboard.add(repay, borrow)
    return keyboard
